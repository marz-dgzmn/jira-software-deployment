## About
An automated deployment of Jira Software Server.

![picture](images/jira_software_server_screenshot.png)

### Built With

* Manjaro Linux 20.2.1
* Ansible 2.10.6
* Vagrant 2.2.14

## Usage

#### Vagrant
`vagrant up ubuntu-20`

#### Ansible
`ansible-playbook -i hosts site.yml`