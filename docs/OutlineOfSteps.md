## Outline of steps:
1. Find resource/documentation for the server setup. (Resouce: https://confluence.atlassian.com/adminjiraserver/installing-jira-applications-on-linux-from-archive-file-938846844.html)
2. Take note of the server's` recommended resource requirements. (e.g. CPU, RAM, Storage, etc.)
3. Take note of dependencies. (e.g. Java, database, etc.)
4. With the reference/documentation, start automating deployment steps on a virtual machine.

## Issues:
* Started out setting up Jira Core, and it turns our it is now only Cloud based. Switched to Jira Software
* Ubuntu 20.04 comes with PostgreSQL version 12 snapshot. Had to manually add PostgreSQL 11 to its apt repository since Jira Software only supports version 10 and 11.
 has been active for [176,600] milliseconds (since [4/18/21 6:43 AM]) to serve the same request for [http://localhost$:8080/secure/RapidBoard.jspa?rapidView=1&quickFilter=2] and may be stuck (configured threshold for this StuckThreadDetectionValve is [120] seconds). There is/are [3] thread(s) in total that are monitored by this Valve and may be stuck
* Encountered an issue pertaining stucked threads.
`WARNING [ContainerBackgroundProcessor[StandardEngine[Catalina]]] org.apache.catalina.valves.StuckThreadDetectionValve.notifyStuckThreadDetected Thread [http-nio-8080-exec-11] (id=[29]) has been active for [176,600] milliseconds (since [4/18/21 6:43 AM]) to serve the same request for [http://localhost:8080/secure/RapidBoard.jspa?rapidView=1&quickFilter=2] and may be stuck (configured threshold for this StuckThreadDetectionValve is [120] seconds). There is/are [3] thread(s) in total that are monitored by this Valve and may be stuck.`
  Tried setting JAVA's minimum and maximum memory to the same value so Jira won't have to wait for JAVA to process the resizing. 
* When importing the backup data, encountered error: `This data appears to be from an older version of Jira. Please upgrade the data and try again. The current version of Jira is at build number '813005', but the supplied backup file was for build number '77001'.`
  Switched to jira sofware version 7.7.0

## Improvements
- Try using docker containers.
- Consider using community playbooks.
- Consider downloading the tar.gz archive file on a computer within the same network for faster retrieval.
- Design the postgresql playbook to be more re-usable for other deployments.