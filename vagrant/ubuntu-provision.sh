#!/bin/bash

# make sure python 2 is installed
# test -e /usr/bin/python || (apt install -y python3-minimal)

# make sure password authentication is enabled
sed -i -e "/^PasswordAuthentication/c\PasswordAuthentication yes" /etc/ssh/sshd_config;

systemctl restart sshd